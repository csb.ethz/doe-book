#===== Estimation and confidence intervals =====
load("data/single_measurement.RData")

#---- Estimation of expectation and variance ----
n = nrow(single_measurement)
## Mean
mu.hat = mean(single_measurement$y)
## Biased variance estimator
var.hat.bias = sum((single_measurement$y-mu.hat)^2) / n
## Unbiased variance estimator
var.hat = var(single_measurement$y)
## Standard deviation
sd.hat = sd(single_measurement$y)




#---- Estimation of covariance and correlation ----
load("data/double_measurement.RData")
cov.hat = cov(double_measurement$First, double_measurement$Second)
corr.hat = cor(double_measurement$First, double_measurement$Second)



#---- Standard errors ----
# Standard error of mean
se.muhat = sd.hat / sqrt(n)
# Standard error of variance
se.varhat = sqrt(2/(n-1)) * var.hat


#---- Confidence intervals ----
# Confidence intervals for mean
## Based on normal approximation
lcl.muhat.norm = mu.hat + qnorm(p=0.025) * se.muhat
ucl.muhat.norm = mu.hat + qnorm(p=0.975) * se.muhat
## Based on t-distribution
lcl.muhat.t = mu.hat + qt(p=0.025, df=n-1) * se.muhat
ucl.muhat.t = mu.hat + qt(p=0.975, df=n-1) * se.muhat
## Alternative using linear regression with no covariates
m = lm(y~1, data=single_measurement)
confint(m)

# Confidence intervals for variance and standard deviation
lcl.varhat = (n-1)*var.hat / qchisq(p=0.975, df=n-1)
ucl.varhat = (n-1)*var.hat / qchisq(p=0.025, df=n-1)
lcl.sdhat = sqrt(lcl.varhat)
ucl.sdhat = sqrt(ucl.varhat)

#---- Comparing two samples ----
load("data/two_vendors.RData")

# Proportion of variances 
n = length(two_vendors$y[two_vendors$vendor=="A"])
var.hat.A = var(two_vendors$y[two_vendors$vendor=="A"])
var.hat.B = var(two_vendors$y[two_vendors$vendor=="B"])
prop.hat = var.hat.A / var.hat.B 
lcl.prop = prop.hat / qf(p=0.975, df1=n-1, df2=n-1)
ucl.prop = prop.hat / qf(p=0.025, df1=n-1, df2=n-1)

# Difference in means
mu.hat.A = mean(two_vendors$y[two_vendors$vendor=="A"])
mu.hat.B = mean(two_vendors$y[two_vendors$vendor=="B"])
Delta.hat = mu.hat.A - mu.hat.B
var.hat.pool = (var.hat.A + var.hat.B) / 2
se.Deltahat = sqrt( 2*var.hat.pool / n )
lcl.Deltahat = Delta.hat + qt(p=0.025, df=2*n-2) * se.Deltahat
ucl.Deltahat = Delta.hat + qt(p=0.975, df=2*n-2) * se.Deltahat

# Difference in dependent samples
# Estimate and test difference between two vendors
# with samples from same 10 mice leads to more precision
# and power compared to previous non-blocked design
# using 20 mice

load("data/paired_vendors.RData")
n.p = nrow(paired_vendors)
muA.p = mean(paired_vendors$A)
muB.p = mean(paired_vendors$B)
s2A.p = var(paired_vendors$A)
s2B.p = var(paired_vendors$B)
cv.p = cov(paired_vendors$A, paired_vendors$B)
s2.p = (s2A.p+s2B.p) / 2
Delta.p = muA.p - muB.p

di.p = paired_vendors$A - paired_vendors$B

# Residual variance
s2e.p = (1/2) * sum((di.p-Delta.p)^2) / (n.p-1)
# Standard error
se.p = sqrt(2*s2e.p)/sqrt(n.p)
# Equivalent calculation 
se.p2 = sqrt( (s2A.p + s2B.p - 2*cv.p) / n.p ) 


lcl.p = Delta.p + qt(0.025, df=n.p-1) * se.p
ucl.p = Delta.p + qt(0.975, df=n.p-1) * se.p

#---- Standardized difference ----
load("data/two_vendors.RData")
## Extract data for A and B
n = length(two_vendors$y[two_vendors$vendor=="A"])
yA = two_vendors$y[two_vendors$vendor=="A"]
yB = two_vendors$y[two_vendors$vendor=="B"]

## Manual calculation
### Estimate pooled standard deviation
var.hat.A = var(yA)
var.hat.B = var(yB)
sd.hat.pool = sqrt( (var.hat.A + var.hat.B) / 2 )

### Estimate Cohen's d
mu.hat.A = mean(yA)
mu.hat.B = mean(yB)
d.hat = (mu.hat.A - mu.hat.B) / sd.hat.pool

### Confidence interval; note n_A=n_B=n
se.dhat = sqrt( (n+n) / (n*n) + d.hat^2 / (2*(n+n)) )
lcl.dhat = d.hat + qnorm(p=0.025) * se.dhat
ucl.dhat = d.hat + qnorm(p=0.975) * se.dhat

## Same using the effectsize package
require(effectsize)
cohens_d(yA, yB)

## For a method to calculate the CI exactly,
## see also code/cohen_exactci.R