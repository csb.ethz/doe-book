# Data and code for "Statistical Design and Analysis of Biological Experiments"

## Introduction

This repository accompanies the textbook _Statistical Design and Analysis of Biological Experiments_ published by Springer, 2021. It contains most of the datasets used in the book as raw `R`-data (.RData) and as comma-separated textfile (.csv), together with `R`-code to reproduce most of the examples. 

## License
MIT license; see `LICENSE` file

## Errata to the book
You can find a list of errors and corrections for the published book here: [Errata (.PDF)](errata/doe-errata.pdf).

## Organization of repository

* `data`: datasets in `R` raw data format. These can be loaded using `R`'s `load()` function.
* `csv`: same datasets in comma-separated format. Use `read.table(<filename>)` from base-`R` or `read_csv(<filename>)` from package `readr`. Note that some additional tweaking of the data might be necessary to reproduce results, such as setting sum or treatment codings, setting a reference level, or rearranging factor levels.
* `code`: `R`-code for few functions presented in the textbook and for sample code to reproduce examples. Analysis code files are named CHAPTER_SECTION.R, e.g., 03_5.R contains the code to reproduce example from section 5 in chapter 3.

## Description of datasets and code
A brief description of the datasets for each chapter.

### Chapter 1: Principles of Experimental Design
Introduces the main ideas of statistical design of experiment and puts experimental design in the broader context of scientific experimentation.

* `two_vendors`: (Table 1.1) Enzyme levels of 20 mice. Measurements of 10 mice use a kit from vendor "A" for sample processing, the other 10 use a kit from vendor "B".

### Chapter 2: Review of Statistical Concepts
Review of basic statistical methods: describing random variation, estimation, hypothesis testing.

__Data sets__
* `single_measurement`: (Table 2.1) Enzyme levels of 10 mice processed with vendor "A".
* `double_measurement`: (Table 2.2, Figure 2.2) Enzyme levels of 10 mice processed with vendor "A", each sample is measured twice, leading to two values per mouse that are highly correlated.
* `confint_samples`: (Figure 2.6) 10 replicates of samples of 10 enzyme levels to demonstrate confidence intervals
* `two_vendors`: (Table 2.3) Enzyme levels of 20 mice. Measurements of 10 mice use a kit from vendor "A" for sample processing, the other 10 use a kit from vendor "B".
* `paired_vendors`: (Table 2.4) Enzyme levels of 20 samples from 10 mice. One sample per mouse measured with kit "A", the other with kit "B" leads to paired data.

__Code__
* [(Section 2.2) Probability](code/02_2.R)
* [(Section 2.3) Estimation](code/02_3.R)
* [(Section 2.5) Testing](code/02_4.R)
* [(Notes and Summary 2.5) Exact confidence interval for Cohen's d via noncentrality](code/cohen_exactci.R)

### Chapter 3: Planning for Precision and Power 
Different strategies to increase or maximize power and precision. Introduces power calculations for t-test scenarios,

__Data sets__
* `paired_vendors`: Enzyme levels of 20 samples from 10 mice. One sample per mouse measured with kit "A", the other with kit "B" leads to paired data.

__Code__
* [(Section 3.3) Demonstration of power of blocking in two-sample case](code/03_3.R)
* [(Section 3.5) Power and sample size for t-test; fallacy of observed power calculations](code/03_5.R)
* [(Notes and Summary 3.6) getPowerT: function to calculate t-test power](code/getPowerT.R)

### Chapter 4: Comparing More than Two Groups: One-Way AMOVA
Introduces one-way analysis of variance for modeling means of multiple groups and testing omnibus hypothesis that all group means are equal.

__Data sets__
* `drugs`: Enzyme levels resulting from four different drugs D1-D4 of two classes A (D1,D2) and B (D3,D4). Eight mice each are used in each treatment group.
* `drugs_unbalanced`: Subset of `drugs`, taking first 4,2,1,2 response values for drug D1,D2,D3,D4, respectively.

__Code__
* [(Section 4.3) Three different ways for calculation the ANOVA](code/04_3.R)
* [(Section 4.4) Power analysis for ANOVA](code/04_4.R)
* [(Section 4.5) Built-in ANOVA functions and model specification](code/04_5.R)
* [(Section 4.6) Estimation in unbalanced data](code/04_6.R)
* [(Notes and Summary 4.7) getPowerF: A flexible function to calculate power of F-test](code/getPowerF.R)
* [(Notes and Summary 4.7) ANOVA and linear regression with sum or treatment contrasts](code/04_7.R)


### Chapter 5: Comparing Treatment Groups with Linear Contrasts
The construction, estimation and testing of linear contrasts to compare subsets of groups. Introduced here for one-way ANOVA and later extended to other analyses.

__Data sets__
* `drugs`: Enzyme levels resulting from four different drugs D1-D4 of two classes A (D1,D2) and B (D3,D4). Eight mice each are used in each treatment group.
* `drug_concentrations`: Enzyme levels for one drug at five different concentration levels, with eight observations per concentration.
* `drug_metabolization`: Tumor diameters for different combinations of drugs and inhibitors in a multi-channel microfluidic device. Each treatment `Condition` is a combination of one of three `Drug` treatments and two `Inhibitor` treatments. Data from channel 16 is discarded for the analysis. `Diameter` gives the resulting tumor diameter in micro-meters.

__Code__
* [(Section 5.2) Linear contrasts to compare responses to different drugs](code/05_2.R)
* [(Section 5.2) Helmert contrasts for emmeans](code/helmert.emmc.R)
* [(Section 5.2) Power analysis](code/05_2a.R)
* [(Section 5.3) Multiple comparison procedures using `emmeans`](code/05_3.R)
* [(Section 5.4) Real-life example: Drug metabolization](code/05_4.R)

### Chapter 6: Multiple Treatment Factors: Factorial Designs
Design and analysis of experiments with combinations of several treatment factors.

__Data sets__
* `drugs_diets`: 24 observations of enzyme levels, with 8 mice randomly assigned to three drugs (placebo, D1, D2), and 4 mice of each drug group randomly assigned to two diets (low fat, high fat).
* `drugs_diets_unbalanced`: 9 observations of crossed drug and dieet treatment factors with unbalanced treatment groups.

__Code__
* [(Section 6.3) Manual and automated calculation of two-way ANOVA with interaction](code/06_3.R)
* [(Section 6.4) Analysis of unreplicated factorial design using Lenth's method](code/06_4.R)
* [(Section 6.5) Two-way ANOVA with unbalanced data (type-I vs type-II sums of squares)](code/06_5.R)
* [(Section 6.6) Contrast analysis for two-way ANOVA](code/06_6.R)
* [(Section 6.7) Power and sample size in two-way ANOVA](code/06_7.R)

### Chapter 7: Improving Precision and Power: Blocked Designs
Explains how grouping experimental units by 'similarity' can lead to vastly improved precision and power without increase in sample size.

__Data sets__
* `drugs_litters`: A randomized complete block design with three drugs (Placebo, D1, D2) randomly assigned to members of eight litters of three mice.
* `drugs_diets_litters`: A randomized complete block design with factorial treatment structure: three drugs (Placebo, D1, D2) and two diets (low fat, high fat) randomly assigned to members of four litters of six mice, i.e., full treatment design fits in each block.
* `drugs_batches`: A balanced incomplete block design with three drug treatments, where only two treatments can be evaluated within the same batch.

__Code__
* [(Section 7.2) Analysis of RCBD with ANOVA and linear mixed model, contrast analysis, power analysis, blocked factorial](code/07_2.R)
* [(Section 7.3) Analysis of BIBD](code/07_3.R)

### Chapter 8: Split-Unit Designs
Introduces designs where treatment factors are randomized on different units, leading to a more complex unit structure
with several error strata in the ANOVA.

__Data sets__
* `drugs_diets_split`: A 3x2 factorial treatment design with three drugs and two diets is combined with two nested units: diets are randomized on cages, drugs on mice within cages, leading to a split-unit design.
* `drugs_diets_vendors_split`: A 3x2x2 factorial treatment design with three drugs, two diets and two vendors is combined with two nested units: drugs and diets are randomized on mice, while vendors are randomized on two samples from each mouse. This is a split-unit design with a factorial treatment structure on the whole-unit level.
* `drugs_diets_vendors_splitsplit`: Same 3x2x2 factorial treatment design with three drugs, two diets and two vendors. Here, diets are randomized on cages, drugs on mice within cages, and vendors on samples from each mouse. This adds a further layer of 'splitting' and leads to a split-split-unit design with four error strata.
* `oats`: The famous oats variety dataset. Three oats varieties are combined with four levels of nitrogen. Oats are randomized on plots, nitrogen levels on subplots within plots. The design is replicated in six blocks, making it a blocked split-unit design.
* `multipipette`: 

__Code__
* [(Section 8.2) ANOVA, linear mixed model and contrast analyses for simple split-unit design](code/08_2.R)
* [(Section 8.3) Analysis of Yates' oat variety example](code/08_3.R)
* [(Section 8.4) Variations: split-unit with factorial on whole-unit, split-split-unit, criss-cross designs](code/08_4.R)

### Chapter 9: Many Treatment Factors: Fractional Factorial Designs
Introduces fractional factorial designs for treatment factors with two levels, the $2^{k-p}$-series. Discusses half- and quarter-replicates, analysis of non-replicated designs, and blocking using fractional factorials.

__Data sets__
* `yeast_frf2`: Data from optimization of yeast growth medium with five components: glucose amount (low high), two different amounts of nitrogen source (N1, N2, low/high each), two different amounts of vitamin source (Vit1, Vit2, low/high each). Levels are encoded as $-1/+1$. Design is a $2^{5-1}$-fractional factorial, replicated twice. Due to experimental error, some data from first replicate are dubious and labelled in `Include` column. Measured response is proxy for growth, defined as increase in optical density over a certain period of time.
* `davies`: Well-known dataset of Davies' to demonstrate analysis of unreplicated design.

__Code__
* [(Section 9.4) Analysis of yeast medium 2^{5-1} design.](code/09_4.R)
* [(Section 9.5) Construction of fractional factorial designs manually and using FrF2 package](code/09_5.R)
* [(Section 9.8) Blocking factorial designs](code/09_8.R)

### Chapter 10: Experimental Optimization with Response Surface Methods
Presents a cleaned and simplified version of an actual optimization experiment: find combination of five ingredients that yields a medium with highest growth for yeast cells.

__Data sets__
* `yeast_rsm1`: results from first response surface exploration experiment., Central composite design with axial points on facets and half-replicate for factorial points. Ingredient values in standardized coordinates -1/0/+1.
* `yeast_gradient1`: gradient of steepest ascent with predicted and measured responses
* `yeast_rsm2`: results from second response surface exploration experiment with center point at optimal value of first gradient
* `yeast_gradient2`: new gradient of steepest ascent with predicted and measured responses

__Code__
* [Example code for RSM construction and analysis](code/10.R)

## Maintainer
Hans-Michael Kaltenbach, Computational Systems Biology group, Department for Biosystems Science and Engineering, ETH Zurich, Switzerland
`michael.kaltenbach AT bsse DOT ethz DOT ch`


