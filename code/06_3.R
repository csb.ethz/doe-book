#===== Calculating two-way ANOVA =====
load("data/drugs_diets.RData")

#---- Manual calculation of two-way ANOVA with interaction ----
## Group means
mu = mean(drugs_diets$y)
mu.P = mean(drugs_diets$y[drugs_diets$drug=="Placebo"])
mu.D1 = mean(drugs_diets$y[drugs_diets$drug=="D1"])
mu.D2 = mean(drugs_diets$y[drugs_diets$drug=="D2"])
mu.low = mean(drugs_diets$y[drugs_diets$diet=="low fat"])
mu.high = mean(drugs_diets$y[drugs_diets$diet=="high fat"])
mu.P.low = mean(drugs_diets$y[drugs_diets$drug=="Placebo" & drugs_diets$diet=="low fat"])
mu.P.high = mean(drugs_diets$y[drugs_diets$drug=="Placebo" & drugs_diets$diet=="high fat"])
mu.D1.low = mean(drugs_diets$y[drugs_diets$drug=="D1" & drugs_diets$diet=="low fat"])
mu.D1.high = mean(drugs_diets$y[drugs_diets$drug=="D1" & drugs_diets$diet=="high fat"])
mu.D2.low = mean(drugs_diets$y[drugs_diets$drug=="D2" & drugs_diets$diet=="low fat"])
mu.D2.high = mean(drugs_diets$y[drugs_diets$drug=="D2" & drugs_diets$diet=="high fat"])

## Group and sample sizes
a = 3 # drugs
b = 2 # diets
N = nrow(drugs_diets) # observations
n = N / (a*b) # group size of each treatment group

## Sums of squares
SS.tot = sum((drugs_diets$y-mu)^2)
SS.drug = b * n * ((mu.P-mu)^2 + (mu.D1-mu)^2 + (mu.D2-mu)^2)
SS.diet = a * n * ((mu.low-mu)^2 + (mu.high-mu)^2)
SS.drugdiet = n * ((mu.P.low - mu.P - mu.low + mu)^2 + (mu.P.high - mu.P - mu.high + mu)^2
                   + (mu.D1.low - mu.D1 - mu.low + mu)^2 + (mu.D1.high - mu.D1 - mu.high + mu)^2 
                   + (mu.D2.low - mu.D2 - mu.low + mu)^2 + (mu.D2.high - mu.D2 - mu.high + mu)^2)
SS.res = SS.tot - SS.drug - SS.diet - SS.drugdiet

## Degrees of freedom
df.tot = N - 1
df.drug = a - 1
df.diet = b - 1
df.drugdiet = (a-1)*(b-1)
df.res = a*b*(n-1)

## Mean squares
MS.tot = SS.tot / df.tot
MS.drug = SS.drug / df.drug
MS.diet = SS.diet / df.diet
MS.drugdiet = SS.drugdiet / df.drugdiet
MS.res = SS.res / df.res

## F-tests
F.drug = MS.drug / MS.res
p.drug = 1 - pf(F.drug, df1=df.drug, df2=df.res)

F.diet = MS.diet / MS.res
p.diet = 1 - pf(F.diet, df1=df.diet, df2=df.res)

F.drugdiet = MS.drugdiet / MS.res
p.drugdiet = 1 - pf(F.drugdiet, df1=df.drugdiet, df2=df.res)




#---- Two-way ANOVA in R ----
## From the Hasse diagram (Fig. 6.3), we derive the
## model specification
## 1 + drug + diet + drug:diet + Error(mouse)
m0 = aov(y~1 + drug + diet + drug:diet + Error(mouse), data=drugs_diets)
## This model is equivalent to drug+diet+drug:diet, removing unnecessary specifications 
m1 = aov(y~drug+diet+drug:diet, data=drugs_diets)
## Using R's syntatic sugar, we abbreciate the model to drug*diet
m2 = aov(y~drug*diet, data=drugs_diets)
summary(m0)
summary(m1)
summary(m2)







#---- Effect sizes ----

## Variance explained - eta^2
etasq.drug = SS.drug / SS.tot
etasq.diet = SS.diet / SS.tot
etasq.drugdiet = SS.drugdiet / SS.tot
## Partial eta^2
etapsq.drug = SS.drug / (SS.drug + SS.res)
etapsq.diet = SS.diet / (SS.diet + SS.res)
etapsq.drugdiet = SS.drugdiet / (SS.drugdiet + SS.res)

## Standardized f^2
f2.drug = etapsq.drug / (1 + etapsq.drug)
f2.diet = etapsq.diet / (1 + etapsq.diet)
f2.drugdiet = etapsq.drugdiet / (1 + etapsq.drugdiet)







#---- Estimated marginal means ----
library(emmeans)
## Full model with interaction
m.full = aov(y~drug*diet, data=drugs_diets)
emmeans(m.full, ~drug+diet)

## Additive model without interaction
m.additive = aov(y~drug+diet, data=drugs_diets)
emmeans(m.additive, ~drug+diet)

## Drug only
m.drug = aov(y~drug, data=drugs_diets)
emmeans(m.drug, ~drug)

## Diet only
m.diet = aov(y~diet, data=drugs_diets)
emmeans(m.diet, ~diet)

## Average model
m.avg = aov(y~1, data=drugs_diets)
emmeans(m.avg, ~1)





#---- Real-life example: drug metabolization ----
load("data/drug_metabolization.RData")
drug_metabolization = drug_metabolization[drug_metabolization$Channel!=16, ]
m = aov(Diameter~Drug*Inhibitor, data=drug_metabolization)
summary(m)
