#===== One-way ANOVA and Linear Regression =====

load("data/drugs.RData")
# Fit ANOVA model
m = aov(y~drug, data=drugs)

# Get linear regression parameters
# using treatment contrast with
# reference level 'D1'
summary.lm(m)

# Fit equivalent linear regression model
m.lm = lm(y~drug, data=drugs)
summary(m.lm)
# Get all four parameter estimates (D1 is zero)
dummy.coef(m.lm)
# Get ANOVA table from regression model
summary.aov(m.lm)

# Use a sum contrast instead
m.sum = lm(y~drug, data=drugs, contrasts=list(drug=contr.sum(4)))
summary(m.sum)
dummy.coef(m.sum)

# Use a treatment contrast with
# D3 as reference level
# Option 1: base-R
drugs$drug = relevel(drugs$drug, 3)
m.d3 = aov(y~drug, data=drugs)
summary.lm(m.d3)
dummy.coef(m.d3)

# Option 2: forcats package
# Use 'D2' as reference
require(forcats)
drugs$drug = fct_relevel(drugs$drug, "D2")
m.d2 = aov(y~drug, data=drugs)
summary.lm(m.d2)
dummy.coef(m.d2)
