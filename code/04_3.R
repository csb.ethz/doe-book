#===== Calculating a one-way ANOVA =====

#---- Calculate all entries in a one-way ANOVA table 'by hand' ----
load("data/drugs.RData")

# Number of groups
k = 4
# Number of samples
N = nrow(drugs)
# Number mice per group
n = N / k
# Grand mean
mu = mean(drugs$y)
# Group means
mu.D1 = mean(drugs$y[drugs$drug=="D1"])
mu.D2 = mean(drugs$y[drugs$drug=="D2"])
mu.D3 = mean(drugs$y[drugs$drug=="D3"])
mu.D4 = mean(drugs$y[drugs$drug=="D4"])

# Residual variance via group means
sigma2.tilde = n * ((mu.D1-mu)^2 + (mu.D2-mu)^2 + (mu.D3-mu)^2 + (mu.D4-mu)^2) / (k-1)

# Residual variance via within-group estimates
sq.D1 = sum((drugs$y[drugs$drug=="D1"]-mu.D1)^2)
sq.D2 = sum((drugs$y[drugs$drug=="D2"]-mu.D2)^2)
sq.D3 = sum((drugs$y[drugs$drug=="D3"]-mu.D3)^2)
sq.D4 = sum((drugs$y[drugs$drug=="D4"]-mu.D4)^2)
sigma2.hat = (sq.D1 + sq.D2 + sq.D3 + sq.D4) / (N-k)

# F-test
F.stat.manual = sigma2.tilde / sigma2.hat
F.p = 1- pf(F.stat.manual, df1=k-1, df2=N-k)




#---- ANOVA via sum of squares ----
SS.tot = sum((drugs$y-mu)^2)
SS.trt = n * ((mu.D1-mu)^2 + (mu.D2-mu)^2 + (mu.D3-mu)^2 + (mu.D4-mu)^2)
SS.res = (sq.D1 + sq.D2 + sq.D3 + sq.D4)
df.trt = k-1
df.res = N-k
MS.trt = SS.trt / df.trt
MS.res = SS.res / df.res
F.stat.SS = MS.trt / MS.res




#---- ANOVA via built-in function (cf. manual Table 4.2) ----
m = aov(y~drug, data=drugs)
summary(m)





#---- Effect sizes ----
## Eta squared: variance explained
etasq.trt = SS.trt / SS.tot
# raw effect b^2
b2 = ((mu.D1-mu)^2 + (mu.D2-mu)^2 + (mu.D3-mu)^2 + (mu.D4-mu)^2) / k
# standardized effect f^2
f2 = b2 / MS.res
# estimate of f^2
f2.hat = SS.trt / SS.res




